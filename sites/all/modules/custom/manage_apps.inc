
<?php

function manage_user_app($developer){
	$org=variable_get('apps_org');
    $username=variable_get('apps_username');
    $password=variable_get('apps_password');
    $url = "https://api.enterprise.apigee.com/v1/organizations/" . $org . "/apps?expand=true";
	$credentials = array('username'=>$username,'pass'=>$password);
	$encrypt_credentials = base64_encode($credentials['username'] . ":" . $credentials['pass']);
	$headers = array('Authorization'=>'Basic ' . $encrypt_credentials,'Content-Type'=>'application/json');
	$options = array(
    	'headers' => $headers,
    	'method' => 'GET',
    );
	$response = drupal_http_request($url, $options);

	$response_json = drupal_json_decode($response->data);
	$response_array = $response_json['app'];

$rid=0;
$col=0;
	foreach($response_array as $key=>$value){
	
       foreach($value as $k=>$v){

          if($k=='createdBy'){
           $developer_email=$v;
	       $rows[$rid][]=$v;
	     }
        if($k=='name'){
	        $rows[$rid][]=$v;
	        $appname=$v;
	      }
	    if($k=='attributes'){
	    $rows[$rid][]=$v[3]['value'];
	    $last_modified=$v[3]['value'];
	     
	}
	}
	$status=$value['credentials'][0]['apiProducts'][0]['status'];
	$consumer_key=$value['credentials'][0]['consumerKey'];
	$product=$value['credentials'][0]['apiProducts'][0]['apiproduct'];
	
	 $email=isset($last_modified)? $last_modified : $developer_email;
$rows[$rid][]=$status;
if($status=='pending' || $status=='revoked'){



    $rows[$rid][]=l(t('Approve'), 'apps/'.$email.'/manage-apps/'.$appname.'/approve/'.$consumer_key.'/'.$product, array('attributes' => array('title' => t('Approve app'), 'class' => 'btn link-success')));  
  }
if($status=='approved'){
    $rows[$rid][]=l(t('Revoke'), 'apps/'.$email.'/manage-apps/'.$appname.'/revoke/'.$consumer_key.'/'.$product, array('attributes' => array('title' => t('Revoke app'), 'class' => 'btn link-danger')));  
  }
	$rid++;
}


	
  $header=array('Last Modified','Created By' ,'App Name','Status','Approve/Reject App');
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'permissions')));
  $output .= theme('pager');
  return $output;	

}


function user_app_approve($developer_email,$appname,$consumer_key,$product){
	$action='approve';
	$org=variable_get('apps_org');
    $username=variable_get('apps_username');
    $password=variable_get('apps_password');
    $credentials = array('username'=>$username,'pass'=>$password);
	$encrypt_credentials = base64_encode($credentials['username'] . ":" . $credentials['pass']);
	$headers = array('Authorization'=>'Basic ' . $encrypt_credentials,'Content-Type'=>'application/octet-stream');
	$options = array(
    	'headers' => $headers,
    	'method' => 'POST',
    );

    $url = "https://api.enterprise.apigee.com/v1/organizations/" . $org . "/developers/" . $developer_email . "/apps/" . $appname .'/keys/'.$consumer_key.'/apiproducts/'.$product."?action=" . $action;
	$response = drupal_http_request($url, $options);
	if(isset($appname)){
		if($action=='approve'){
			drupal_set_message(t(" " . $appname . " is Approved"), 'status', FALSE);
		}
}
drupal_goto('apps');
}

function user_app_revoke($developer_email,$appname,$consumer_key,$product){
	$action='revoke';
	$org=variable_get('apps_org');
    $username=variable_get('apps_username');
    $password=variable_get('apps_password');
    $credentials = array('username'=>$username,'pass'=>$password);
	$encrypt_credentials = base64_encode($credentials['username'] . ":" . $credentials['pass']);
	$headers = array('Authorization'=>'Basic ' . $encrypt_credentials,'Content-Type'=>'application/octet-stream');
	$options = array(
    	'headers' => $headers,
    	'method' => 'POST',
    );

    $url = "https://api.enterprise.apigee.com/v1/organizations/" . $org . "/developers/" . $developer_email . "/apps/" . $appname .'/keys/'.$consumer_key.'/apiproducts/'.$product."?action=" . $action;
	$response = drupal_http_request($url, $options);
	if(isset($appname)){
		if($action=='revoke'){
			drupal_set_message(t(" " . $appname . " is Revoked"), 'status', FALSE);
		}
}
drupal_goto('apps');
}