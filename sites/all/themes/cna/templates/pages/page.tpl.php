<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in the ../system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * CUSTOMIZATIONS:)</
 * - $myappslink: provides a link for the users my apps page (with glyphicon)
 * - $profilelink: provides a link for the user profile page (with glyphicon)
 * - $logoutlink: provides a link for the user to log out (with glyphicon)
 * - $company_switcher: Provides the dropdown to switch companies if
 *   apigee_company module is enabled.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

 $current_path = $_SERVER['REQUEST_URI'];
?>

<!-- add a disclaimer here -->

<!-- Header -->
<?php if (drupal_is_front_page()): ?>
  <header id="header" role="banner" class="alt">
<?php else: ?>
  <header id="header" role="banner">
<?php endif; ?>

<a href="/"><img src="<?php print $logo; ?>" /></a>
<nav id="nav">
  <?php print $primary_navigation; ?>
</nav>
</header>

<?php if (drupal_is_front_page()): ?>

<!-- Banner -->

   <?php if (in_array('Partner',$user->roles)):  ?>
       		  <section id="banner" style="overflow: hidden;" class="partnerbanner">
	 <?php else: ?>
						 <section id="banner" style="overflow: hidden;">
	<?php endif; ?>
	              <div class="content">
                    <div class="inner">
                        <header>
                            <h2><?php print $site_name; ?> Developer Portal</h2>
                            <p>CNA Financial Corporation is a financial corporation based in Chicago, Illinois, United States. Its principal subsidiary, Continental Casualty Company (CCC), was founded in 1897. CNA, the current parent company, was incorporated in 1967.

                           </p>
                        </header>
                        <ul class="actions">
                            <li style="padding-right: 0px !important; margin: 10px;"><a href="/user/register" class="button special huge">Request An Access Key</a></li>
                            <li style="margin: 10px;"><a href="/apis" class="button huge">Explore API Docs</a></li>
                        </ul>
                    </div>
                </div>
            </section>

        <!-- One -->
            <section id="one" class="wrapper">
                <div class="inner">
                    <!--
                    <ul class="features">
                        <li>
                            <h3 class="icon style1 fa-area-chart">Integer vitae consequat sed magna feugiat</h3>
                            <p>CNA Financial Corporation is a financial corporation based in Chicago, Illinois, United States. Its principal subsidiary, Continental Casualty Company (CCC), was founded in 1897.[2] CNA, the current parent company, was incorporated in 1967.</p>
                        </li>
                        <li>
                            <h3 class="icon style2 fa-pie-chart">Phasellus libero eu sed augue gravida</h3>
                            <p>CNA is the eighth largest commercial insurer in the United States. CNA provides a broad range of standard and specialized property and casualty insurance products and services for businesses and professionals in the U.S., Canada, Europe and Asia, backed by 120 years of experience and more than $45 billion of assets. For more information about CNA visit www.cna.com.</p>
                        </li>
                        <li>
                            <h3 class="icon style3 fa-cube">Lacus ultricies sagittis mi dui dapibus</h3>
                            <p>Lorem ipsum dolor sit amet interdum mollis sapien. Sed ac risus. Phasellus lacinia, ullamcorper laoreet, lectus arcu pulvinar lorem ipsum interdum sed tempus sagittis lorem feugiat. In fringilla diet consectetur. Morbi libero orci, consectetur in odio maximus felis.</p>
                        </li>
                    </ul>
                    -->
                    <?php print render($page['content']); ?>
                </div>
            </section>

        <!-- Two -->
            <section id="two" class="wrapper cta">
            	<div class="inner">
                    <header>
                        <h2><?php print $site_name; ?> Approach, Purpose</h2>
                        <ul class="actions">
                            <li style="padding-left: 0px !important;"><a href="http://dev-cna-eval.devportal.apigee.io/" target="_blank" class="button big">Read More</a></li>
                        </ul>
                    </header>
                    <p>
                        CNA Financial Corporation is a financial corporation based in Chicago, Illinois, United States. Its principal subsidiary, Continental Casualty Company (CCC), was founded in 1897.[2] CNA, the current parent company, was incorporated in 1967.

                        CNA is the eighth largest commercial insurer in the United States. CNA provides a broad range of standard and specialized property and casualty insurance products and services for businesses and professionals in the U.S., Canada, Europe and Asia, backed by 120 years of experience and more than $45 billion of assets. For more information about CNA visit www.cna.com.

                    </p>
                </div>
	    </section>

        <!-- Three -->
            <section id="three" class="wrapper three">
                <div class="inner">
                    <header class="major">
                        <h2>Search</h2>
                        <p>Find published API products (ie. APIs ready to be consumed), forum posts, & other useful content</p>
                    </header>
                    <div class="big-search-form">
                        <form method="post" id="search-form" action="/search/node" accept-charset="UTF-8">
                            <input placeholder="[type your search here, then hit enter]" type="text" name="keys" id="edit-keys" />
                            <input type="hidden" name="form_id" value="search_form" />
                            <input type="submit" value="submit" style="visibility: hidden;" />
                            <div style="clear: both; height: 1px; overflow: hidden;">&nbsp;</div>
                        </form>
                    </div>
                </div>
            </section>

<?php else: ?>

        <?php $extra_class = (empty($page['sidebar_first']) ? '' : "sidebar left"); $extra_class = ''; ?>
        <div class="master-container"><div class="wrapper <?php echo $extra_class; ?>">

            <div class="inner">

                <!-- Breadcrumbs -->
                <?php if ($breadcrumb): ?>
                <div class="container" id="breadcrumb-navbar" style="width: 100%;">
                    <div class="row">
                        <br/>
                        <div class="col-md-12"><?php print $breadcrumb;?></div>
                    </div>
                </div>
                <?php endif; ?>

                <section class="page-header"><header class="major">

                    <!-- Title -->
                    <?php print render($title_prefix); ?>
                    <h2><?php print render($title); ?></h2>

                    <!-- Subtitle -->
                    <?php if (!empty($subtitle)): ?>
                    <br/>
                    <p class="subtitle">
                        <span class="text-muted">
                            <span class="glyphicon glyphicon-info-sign"></span>
                            <?php print render($subtitle); ?>
                        </span>
                    </p>
                    <?php endif; ?>

                    <!-- Title Suffix -->
                    <?php print render($title_suffix); ?>

                </header></section>

                <div class="content">
                    <?php if ($messages): ?>
                    <div class="row messages-row">
                        <div class="col-md-12">
                            <?php print $messages; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if (!empty($tabs) && !drupal_is_front_page()): ?>
                        <?php print render($tabs); ?>
                    <?php endif; ?>
                    <?php if (!empty($page['help'])): ?>
                        <?php print render($page['help']); ?>
                    <?php endif; ?>
                    <?php if (!empty($action_links)): ?>
                        <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>

                    <?php print render($page['content']); ?>
                </div>

            </div>
        </div></div>
<?php endif; ?>

        <!-- Footer -->
            <footer id="footer">
                <div class="content">
                    <div class="inner">
                        <section class="about">
                        <h3><?php print $site_name; ?> API Developer Portal</h3>
                            <p>Curious how this came together? Interested in contributing? Contact an API product team near you :)</p>
                            <ul class="actions">
                                <li style="padding-left: 0px !important;"><a href="#" class="button">Learn More</a></li>
                            </ul>
                        </section>
                        <ul class="icons bulleted">
                            <li class="icon fa-map-marker">
                                <h3>Address</h3>
                                <p>API Team Central<br />
                                1234 Abcd Road<br />
                                Chicago, IL 00000</p>
                            </li>
                        </ul>
                        <ul class="icons bulleted">
                            <li class="icon fa-phone">
                                <h3>Phone</h3>
                                <p>(000) 000-0000</p>
                            </li>
                            <li class="icon fa-envelope-o">
                                <h3>Email</h3>
                                <p><a href="#">api-portal-team@hq.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <p class="copyright">&copy; Google, Inc. Demonstration Portal Only - Not a Sactioned Customer Portal.<br />
                <a href="https://cloud.google.com/apigee-api-management/">Apigee 101</a> | <a href="https://community.apigee.com">Apigee Community</a> | <a href="https://www.youtube.com/results?search_query=4mv4d">Apigee 4MV4D</a></p>
            </footer>

